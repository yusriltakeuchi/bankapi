import pymysql.cursors
from module.conn import Connection
from account.norek import NomorRekening
from account.saldo import Saldo
import datetime

class Transfer:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def Send(self, from_norek, to_norek, amount, notes):
        cursor = self.connection.cursor()

        if amount > 0:
            rekening = NomorRekening()
            #If rekening number sender valid
            if rekening.isExists(from_norek):
                #If rekening number reciver valid
                if rekening.isExists(to_norek):

                    #Get saldo nasabah pengirim
                    data = Saldo()
                    data_saldo_sender = data.Check(from_norek)
                    data_saldo_receiver = data.Check(to_norek)

                    saldo_sender = data_saldo_sender[2]['saldo']
                    saldo_receiver = data_saldo_receiver[2]['saldo']

                    #Logic flow
                    if saldo_sender >= amount:
                        saldo_sender -= amount
                        saldo_receiver += amount

                        temp_date = datetime.datetime.now()
                        date = temp_date.strftime("%Y-%m-%d %H:%M")

                        cursor.execute('INSERT INTO transfer (from_norek, to_norek, saldo, notes, date) values(%s, %s, %s, %s, %s)', (from_norek, to_norek, amount, notes, date))
                        self.connection.commit()

                        #Update saldo sender
                        data.Update(from_norek, saldo_sender)
                        #Update saldo receiver
                        data.Update(to_norek, saldo_receiver)

                        return 'Success', 'Successfully transfer saldo'
                    else:
                        return 'Failed', 'Money is not enough'
                else:
                    return 'Failed', 'Receiver Rekening Number is wrong'
            else:
                return 'Failed', 'Sender Rekening Number is wrong'
        else:
            return 'Failed', 'Money cant less than zero'

