from Crypto.Cipher import AES
import base64

class Crypter:

    def __init__(self, text, secret_key):
        self.text = text
        self.secret_key = secret_key

    def Encrypt(self):
        text = self.text.rjust(32)
        chiper = AES.new(self.secret_key, AES.MODE_ECB)

        return base64.b64encode(chiper.encrypt(text))

    def Decrypt(self):
        chiper = AES.new(self.secret_key, AES.MODE_ECB)
        decoded = chiper.decrypt(base64.b64decode(self.text))
        return decoded.strip()