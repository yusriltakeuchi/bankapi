# Bank API
![alt text](https://i.ibb.co/7rLx3r8/Selection-166.png)

API ini berfungsi sebagai api banking system yang mempunyai fitur:
- Create Nasabah Account
- Setor Saldo
- Penarikan Saldo
- Transfer Saldo
- Nomor Rekening (15 Digit Angka)
- Check Rekening (Personal or All)
- Login & Register System
- Apikey System

# Installation
Pastikan kalian telah menginstall python3 dan pip3.
```
pip3 install -r requirements.txt
```

# API Route
>>>
Untuk sisi keamanan Password & Generate Apikey menggunakan Block chiper AES 16 byte.
>>>

```
Rute API:
Main Route -> Controller -> Core System
```
```
After Success Get Data:
Core System -> Controller -> Main Route -> Display
```

```
Fungsi Controller:
Controller -> Menentukan Rute, Validasi Apikey, mengambil data dari Core, mengirim ke Main Route
```

# API Creator 
```
Author: Yusril Rapsanjani
Version: v1.0
Website: www.yurani.me
```

# API Requirements
| Features | Methods | Apikey | Secret Key |
| :--------: | :--------: | :---------: | :-------: |
| Generate Keys   | POST   | No | Yes |
| Create Nasabah   | POST   | Yes | No |
| Login Nasabah   | GET   | Yes | No |
| Withdraw   | POST   | Yes | No |
| Deposit   | POST   | Yes | No |
| Transfer   | POST   | Yes | No |
| Check Rekening   | GET   | Yes | No |
| Check Rekening All   | GET   | Yes | No |

# API Usage List
__Generate Apikeys__
---
```
Rute ini berfungsi untuk melakukan generate APIKEY.
Untuk melakukannya, kita perlu memasukkan parameter secret key.
Hal ini mencegah dari orang yang membypass generate apikey, jadi
hanya orang tertentu yang tau secret key nya saja yang bisa generate apikey

Secret key => 1129874678923456
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| POST   | secret_key   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/keys/generate
```

__Code Backend__
```python
@app.route('/api/v1/keys/generate', methods=['POST'])
def generateToken():

    if 'secret_key' in request.args:
        secret = request.args['secret_key']
        controller = Apikey_Controller(secret)
        data = controller.Generate()

        return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/y0HtVr5/Selection-161.png)
---

__Create Nasabah__
---
```
Rute ini berfungsi untuk membuat akun nasabah.
Perlu diingatkan bahwa kalian memerlukan APIKEY.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| POST   | username   | String |
| POST   | password   | String |
| POST   | email   | String |
| POST   | phone   | String |
| POST   | address   | String |
| POST   | firstname   | String |
| POST   | lastname   | String |
| POST   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/create
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/create', methods=['POST'])
def createNasabah():

    #Validasi parameter
    if 'username' in request.args and 'password' in request.args and 'email' in request.args and 'phone' in request.args and 'address' in request.args and 'firstname' in request.args and 'lastname' in request.args and 'keys' in request.args:
        username = request.args['username']
        password = request.args['password']
        email = request.args['email']
        phone = request.args['phone']
        address = request.args['address']
        firstname = request.args['firstname']
        lastname = request.args['lastname']
        apikey = request.args['keys']

        controller = Nasabah_Controller(apikey)
        data = controller.Create(username, password, email, phone, address, firstname, lastname)

        return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/KsmT5BC/Selection-155.png)
---

__Login Nasabah__
---
```
Rute ini untuk melakukan validasi login.
Pastikan kalian memasukan parameter keys, karena
kita tidak ingin validasi ini digunakan sembarangan orang.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| GET   | username   | String |
| GET   | password   | String |
| GET   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/login?username=yusril&password=P@ssw0rd&keys=<yourapikey>
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/login', methods=['GET'])
def loginNasabah():
    if 'keys' in request.args and 'username' in request.args and 'password' in request.args:
        username = request.args['username']
        password = request.args['password']
        apikey = request.args['keys']

        controller = Nasabah_Controller(apikey)
        data = controller.Login(username, password)

        return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/7G7sc3t/Selection-162.png)

__Withdraw__
---
```
Rute ini untuk melakukan withdraw uang atau pengambilan uang
dari akun nasabah. Pastikan bahwa nomor rekening benar.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| POST   | norek   | String |
| POST   | amount   | Integer |
| POST   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/withdraw
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/withdraw', methods=['POST'])
def withdraw():
        if 'norek' in request.args and 'keys' in request.args and 'amount' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']
                amount = int(request.args['amount'])

                controller = Transaction_Controller(apikey)
                data = controller.Take(norek, amount)

                return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/gyCp2ZP/Selection-163.png)

__Deposit__
---
```
Rute ini untuk melakukan deposit uang ke dalam akun nasabah.
Pastikan bahwa nomor rekening benar.
Nomor Rekening terdiri dari 15 Digit
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| POST   | norek   | String |
| POST   | amount   | Integer |
| POST   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/deposit
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/deposit', methods=['POST'])
def deposit():
        if 'norek' in request.args and 'keys' in request.args and 'amount' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']
                amount = int(request.args['amount'])

                controller = Transaction_Controller(apikey)
                data = controller.Insert(norek, amount)

                return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/fNhN1WL/Selection-157.png)

__Transfer__
---
```
Rute ini berfungsi untuk melakukan transfer saldo antara nasabah.
Pastikan bahwa nomor rekening pengirim dan nomor rekening tujuan benar.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| POST   | from_norek   | String |
| POST   | to_norek   | String |
| POST   | amount   | Integer |
| POST   | notes   | String |
| POST   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/transfer
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/transfer', methods=['POST'])
def transfer():
        if 'from_norek' in request.args and 'to_norek' in request.args and 'amount' in request.args and 'notes' in request.args and 'keys' in request.args:
                from_norek = request.args['from_norek']
                to_norek = request.args['to_norek']
                amount = int(request.args['amount'])
                notes = request.args['notes']
                apikey = request.args['keys']
                
                controller = Transaction_Controller(apikey)
                data = controller.Send(from_norek, to_norek, amount, notes)
                
                return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/YW46Frb/Selection-164.png)

__Check Rekening__
---
```
Rute ini berfungsi untuk melakukan pengecekan rekening. 
Dengan memasukkan nomor rekening dan apikey, kita bisa melihat
detail informasi nasabah seperti profile dan saldonya.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| GET   | norek   | String |
| GET   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/check?norek=658233202693885&keys=<yourapikeys>
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/check', methods=['GET'])
def checkRekening():
        if 'norek' in request.args and 'keys' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']

                controller = Saldo_Controller(norek, apikey)
                data = controller.Check()

                return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/SB78hD7/Selection-156.png)

__Check Rekening All__
---
```
Rute ini berfungsi untuk melakukan pengecekan rekening ke semua nasabah. 
Dengan memasukkan nomor rekening dan apikey, kita bisa melihat
detail informasi nasabah seperti profile dan saldonya.
```

| Methods | Parameter | Data Type |
| :--------: | :--------: | :---------: |
| GET   | keys   | String |

__URL String__
```
http://127.0.0.1:5000/api/v1/nasabah/checkall?keys=<yourapikeys>
```

__Code Backend__
```python
@app.route('/api/v1/nasabah/checkall', methods=['GET'])
def checkRekeningAll():
        if 'keys' in request.args:
                apikey = request.args['keys']

                controller = Saldo_Controller('', apikey)
                data = controller.CheckAll()

                return jsonify(data)
```

__API Response__

![alt text](https://i.ibb.co/BqDGnPy/Selection-160.png)

# Database Installation
1. Buat database baru pada MySQL (ex: banking)
2. Klik menu __Import__ pada Phpmyadmin
3. Klik __Browse__ dan pilih __banking.sql__ pada folder __SQL__ lalu tekan tombol __Go__
4. Ubah Database Configuration pada file __module/conn.py__

```python
class Connection:
    def get(self):
        # Connect to the database
        connection = pymysql.connect(host='localhost',
                                    user='root',
                                    password='DBPassword',
                                    db='banking',
                                    charset='utf8mb4',
                                    cursorclass=pymysql.cursors.DictCursor)

        return connection
```

# Donations
Bagi yang mau berbaik hati mendonasikan saldonya pada saya atas script ini, silahkan melakukan transfer ke rekening berikut:
```
Transfer Rekening => 2850031684 A/N Muhammad Yusril Rapsanjani | Bank BCA
```